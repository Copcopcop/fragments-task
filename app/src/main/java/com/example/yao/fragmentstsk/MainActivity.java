package com.example.yao.fragmentstsk;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;


public class MainActivity extends Activity implements Fragment1.OnFragmentInteractionListener,
        FragmentButton.OnFragmentInteractionListener, AdapterView.OnItemClickListener {

    Fragment1 fragment1;
    FragmentButton button;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragment1 = new Fragment1();

        getFragmentManager().beginTransaction()
                .add(R.id.f1, fragment1)
                .commit();

        button = new FragmentButton();

        getFragmentManager().beginTransaction()
                .add(R.id.f2, button)
                .commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onFragmentInteraction(String str) {

    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        count++;
        Button b = (Button) findViewById(R.id.list_remove);
        b.setText("Clicked" + count);
    }
}
